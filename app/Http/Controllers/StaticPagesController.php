<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticPagesController extends Controller

{
    public function waterproofing(){
        return view('waterproofing');
    }

    public function rising(){
        return view('rising');
    }

    public function painting(){
        return view('painting');
    }

     public function water(){
        return view('water');
    }

     public function rope(){
        return view('rope');
    }
     public function tiling(){
        return view('tiling');
    }

     public function stealframing(){
        return view('stealframing');
    }

     public function gallery(){
        return view('gallery');
    }


    public function contact(){
        return view('contact');
    }


    public function tile(){
        return view('tile');
    }

     public function torch(){
        return view('torch');
    }
     public function acrylic(){
        return view('acrylic');
    }

     public function bitumen(){
        return view('bitumen');
    }

     public function ceiling(){
        return view('ceiling');
    }


    public function roof(){
        return view('roof');
    }



}

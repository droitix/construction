
<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>Contact Us</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Playfair|Open+Sans|Montserrat|Roboto+Slab'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/stroke-gap.css" media="screen">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="7stroke/css/pe-icon-7-stroke.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">


    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>

</head>
<body>

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f12de627258dc118bee7a98/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

 @include('partials.navigation')

        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Contact Us</h2>
                        <ul class="page-depth">
                            <li><a href="index.html">construct</a></li>
                            <li><a href="about.html">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- contact section
            ================================================== -->
        <section class="contact-section">
            <div class="container">
                <div class="row welcome_heading">
                        <h2>Get in Touch</h2>
                        <p>Call us or send an email , we will respond emmediately.</p>
                </div>
                <div class="col-md-8">
                    <form id="contact-form">
                        <div class="row">
                            <div class="col-md-6">
                                <input name="name" id="name" type="text" placeholder="Name">
                            </div>
                            <div class="col-md-6">
                                <input name="mail" id="mail" type="text" placeholder="Email">
                            </div>
                        </div>
                        <textarea name="comment" id="comment" placeholder="Message"></textarea>
                        <input type="submit" id="submit_contact" value="Send Message">
                        <div id="msg" class="message"></div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="contact-info">
                        <h3>Contact Info</h3>
                        <p>You can contact or visit us in our office from Monday to Friday from 8:00 - 17:00</p>
                        <ul class="information-list">
                            <li><i class="fa fa-map-marker"></i><span>76 Carravelle Ave, Impala Park , Boksburg.</span></li>
                            <li><i class="fa fa-phone"></i><span>0744825608</span><span>0815738603</span></li>
                            <li><i class="fa fa-envelope-o"></i><a href="#">roofwaterproofing2020@gmail.com</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </section>
        <!-- End contact section -->
                <!-- map
            ================================================== -->
        <div id="map"></div>
        <!-- map -->


       @include('partials.footer')

    </div>
    <!-- End Container -->



    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <script type="text/javascript" src="js/gmap3.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
</body>
</html>

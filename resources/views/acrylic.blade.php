
<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>Acrylic Systems</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Playfair|Open+Sans|Montserrat|Roboto+Slab'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/stroke-gap.css" media="screen">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/pe-icon-7-stroke.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="screen">




</head>
<body>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f12de627258dc118bee7a98/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

    @include('partials.navigation')
        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Acrylic Systems</h2>
                        <ul class="page-depth">

                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- services-page section
            ================================================== -->
        <section class="services-page-section">
            <div class="container">
                <div class="row">

                    <div class="col-md-9 white-right">
                        <div class="services-wrapp">
                            <div class="services-post">
                                <img src="images/acrylic.jpg" alt="">
                                    <div class="project-title ">
                    <h2 class="box-header">Acrylic Systems</h2>

                </div>
                                <p>Want to restore your tile, corrugated iron or asbestos roof to it former glory? A high-quality acrylic, glossy-finish paint will do just that.

Our acrylic polymers have been engineered specifically for roof applications where house paints would simply be too brittle. The system is UV resistant, waterproof and offers advanced protection from sun damage.

After preparing the roof, we apply the waterproofing acrylic and then a protective membrane. You can choose either to restore the original colour of the roof or opt for a colour upgrade, with countless colour options available.
</p>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="section-title-style-2">


                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="services-post">
                                        <img src="upload/others/con2.jpg" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End services-page section -->


        @include('partials.footer')
    </div>
    <!-- End Container -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>

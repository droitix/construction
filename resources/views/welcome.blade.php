
<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>RWS | Home</title>

    <meta charset="utf-8">
    <link rel="icon" href="images/flavico_io/flavicon.ico" type="image/gif" sizes="16x16">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Playfair|Open+Sans|Montserrat|Roboto+Slab'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/stroke-gap.css" media="screen">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/pe-icon-7-stroke.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="screen">




</head>
<body>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f12de627258dc118bee7a98/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

   @include('partials.navigation')

        <!-- home-section
            ================================================== -->
        <section id="home-section" class="slider1">

            <!--
            #################################
                - THEMEPUNCH BANNER -
            #################################
            -->
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>    <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
                            <!-- MAIN IMAGE -->
                            <img src="images/1.jpg"  alt="slidebg1" data-lazyload="images/1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->


                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption finewide_medium_white lfl tp-resizeme rs-parallaxlevel-0"
                                data-x="620"
                                data-y="130"
                                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="500"
                                data-start="1200"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">WATERPROOFING<br><span>&amp; STEEL FRAMING</span> <br>
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption small_text customin tp-resizeme rs-parallaxlevel-0"
                                data-x="620"
                                data-y="250"
                                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="500"
                                data-start="1800"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.05"
                                data-endelementdelay="0.1"
                                style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">We are a top level waterproofing,<br> lateral damp proofing specialists,<br> and we do joint sealing, rope access and specialised tiling.
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption lfl tp-resizeme rs-parallaxlevel-0"
                                data-x="620"
                                data-y="370"
                                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="500"
                                data-start="2400"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-linktoslide="next"
                                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='trans-btn'>load more</a>
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption lfr tp-resizeme rs-parallaxlevel-0"
                                data-x="820"
                                data-y="370"
                                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="500"
                                data-start="2500"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-linktoslide="next"
                                style="z-index: 11; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='trans-btn2'>Portfolio</a>
                            </div>

                        </li>

                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-saveperformance="on"  data-title="Ken Burns Slide">
                            <!-- MAIN IMAGE -->
                            <img src="images/dummy.png"  alt="2" data-lazyload="images/paintbrush.jpg" data-bgposition="right top" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center bottom">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption small_text customin tp-resizeme rs-parallaxlevel-0"
                                data-x="430"
                                data-y="190"
                                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="500"
                                data-start="1200"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.05"
                                data-endelementdelay="0.1"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">High grade and specialised
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption finewide_medium_white lfl tp-resizeme rs-parallaxlevel-0"
                                data-x="265"
                                data-y="240"
                                data-speed="500"
                                data-start="1800"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.05"
                                data-endelementdelay="0.05"
                                style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">lateral DAMP PROOFING
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption finewide_medium_white lfr tp-resizeme rs-parallaxlevel-0"
                                data-x="185"
                                data-y="290"
                                data-speed="500"
                                data-start="1800"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;"><span>Painting &amp; Specialised Tiling </span>
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
                                data-x="470"
                                data-y="385"
                                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="500"
                                data-start="2400"
                                data-easing="Power3.easeInOut"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-linktoslide="next"
                                style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='trans-btn'>View Projects</a>
                            </div>
                        </li>
                        <!-- SLIDE  -->

                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </section>
        <!-- End home section -->

        <!-- service box
            ================================================== -->
            <section class="service-box">
            <div class="container">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="service-icon">
                        <img class="img-responsive" class="rounded-circle" src="images/mobile.png" alt="images">
                        </div>
                        <a href=""><h3>Roofing and waterproofing</h3></a>


                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 highlight">
                        <div class="service-icon">
                        <img class="img-responsive" src="images/damp.png" alt="images">
                        </div>
                        <a href=""><h3>RISING LATERAL DAMP PROOFING</h3></a>

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="service-icon">
                        <img class="img-responsive" src="images/icon4.png" alt="images">
                        </div>
                        <a href=""><h3>Preparation and Painting</h3></a>

                    </div>
            </div>
            </section>



            <section class="service-box">
            <div class="container">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="service-icon">
                        <img class="img-responsive" src="images/beam.png" alt="images">
                        </div>
                        <a href=""><h3>Steel Framing</h3></a>

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 highlight">
                        <div class="service-icon">
                        <img class="img-responsive" src="images/sealant.png" alt="images">
                        </div>
                        <a href=""><h3>Joint Sealing</h3></a>

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="service-icon">
                        <img class="img-responsive" src="images/tileicon.png" alt="images">
                        </div>
                        <a href=""><h3>Tiling</h3></a>

                    </div>
            </div>
            </section>

                <!-- about box
            ================================================== -->
            <section class="about-box">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h2 class="left-heading">About Us</h2>
                    <p>We are a leading waterproofing contractors. Originally established in Johannesburg, we’ve since helped hundreds of homeowners, architects and property developers nationwide ensure their buildings become and remain waterproof. Maintenance Aid also offers cutting-edge damp proofing and painting services, as well as specialised tiling and floor polishing.<br>

                     </p>

                    <div class="tl-spoiler col-md-6 pad0 iconbox">
                        <h6 class="ok-icon"> <i class="fa iconbox-icon fa-lightbulb-o" aria-hidden="true"></i> &nbsp; &nbsp; BRILLIANT IDEAS</h6>
                  </div>
                  <div class="tl-spoiler col-md-6 pad0 iconbox">
                        <h6 class="ok-icon"> <i class="fa iconbox-icon fa-user" aria-hidden="true"></i> &nbsp; &nbsp; PROFESSIONAL SPECIALISTS</h6>
                  </div>
                  <div class="tl-spoiler col-md-6 pad0 iconbox">
                        <h6 class="ok-icon"> <i class="fa iconbox-icon fa-globe" aria-hidden="true"></i> &nbsp; &nbsp; COUNTRYWIDE BRANCH</h6>
                  </div>
                  <div class="tl-spoiler col-md-6 pad0 iconbox">
                        <h6 class="ok-icon"> <i class="fa iconbox-icon fa-bell-o" aria-hidden="true"></i> &nbsp; &nbsp; REVOLUTION</h6>
                        <br><br>
                  </div>
                  <a class="btn btn-theme readmore" href="single-post.html">read more</a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <img src="images/aboutus.png" alt="construct pro">
                </div>

            </div>
            </section>
        <!-- about section
            ================================================== -->
        <section class="about-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 who-content">
                        <div class="we_are_deatails ">
                    <h2 class="left-heading">Who We Are</h2>
                    <p>No matter what the surface or façade. No matter what the building. Whether it’s repairing a leak, retiling a roof or painting a wall. With our market-leading products, highly skilled craftsmen, and guaranteed long-lasting solutions, we bring a vibrancy and endurance to the walls and roofs of homes, industrial plants, residential complexes, modern office blocks and even reservoirs..</p>

<div class="tl-spoiler col-md-6 pad0">
                        <h6 class="ok-icon"> <i class="fa fa-bullhorn" aria-hidden="true"></i> &nbsp; &nbsp; WE ARE PASSIONATE</h6>
                  </div>
                  <div class="tl-spoiler col-md-6 pad0">
                        <h6 class="ok-icon"> <i class="fa fa-heart" aria-hidden="true"></i> &nbsp; &nbsp; HONEST AND DEPENDABLE</h6>
                  </div>
                  <div class="tl-spoiler col-md-6 pad0">
                        <h6 class="ok-icon"> <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> &nbsp; &nbsp; 100% Client Satisfaction</h6>
                  </div>
                  <div class="tl-spoiler col-md-6 pad0">
                        <h6 class="ok-icon"> <i class="fa fa-user" aria-hidden="true"></i> &nbsp; &nbsp; Awesome Support</h6>
                  </div>

                </div>
                </div>
                    <div class="col-md-6 who-img"><img class="img-full" src="images/5.jpg" alt=""></div>

                </div>
            </div>
        </section>
        <!-- End about section -->



@include('partials.footer')
    </div>
    <!-- End Container -->
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.migrate.js"></script>
    <script type="text/javascript" src="/js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="/js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="/js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="/js/jquery.countTo.js"></script>
    <script type="text/javascript" src="/js/script.js"></script>
</body>
</html>

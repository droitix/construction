
<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>Construction</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Playfair|Open+Sans|Montserrat|Roboto+Slab'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/stroke-gap.css" media="screen">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/pe-icon-7-stroke.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="screen">




</head>
<body>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f12de627258dc118bee7a98/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
   @include('partials.navigation')

        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Light Gauge Steel Framing Housing</h2>
                        <ul class="page-depth">

                            <li><a href="about.html"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- news-section
            ================================================== -->
        <section class="news-section news-section-home">
            <div class="container">

                <div class="news-box">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <article>
                                <div class="news-post">
                                <div class="img-cap-effect">
                    <div class="img-box">
                        <img src="images/1.png" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">Stage 1</a>
                                </div>
                            </div>
                            </article>

                        </div>
             <div class="col-md-4 col-sm-6">
                <article>
                    <div class="news-post">
                        <div class="img-cap-effect">
                        <div class="img-box">
                        <img src="images/2.png" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">Stage 2</a>
                                </div>
                            </div>
                            </article>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <article>
                            <div class="news-post">
                                                                <div class="img-cap-effect">
                    <div class="img-box">
                        <img src="images/3.png" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">Stage 3</a>
                                </div>
                            </div>
                            </article>
                        </div>

                    </div>




                     <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <article>
                                <div class="news-post">
                                <div class="img-cap-effect">
                    <div class="img-box">
                        <img src="images/4.jpg" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">Stage 4</a>
                                </div>
                            </div>
                            </article>

                        </div>
             <div class="col-md-4 col-sm-6">
                <article>
                    <div class="news-post">
                        <div class="img-cap-effect">
                        <div class="img-box">
                        <img src="images/5.png" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">5</a>
                                </div>
                            </div>
                            </article>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <article>
                            <div class="news-post">
                                                                <div class="img-cap-effect">
                    <div class="img-box">
                        <img src="images/aboutus.png" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">View more</a>
                                </div>
                            </div>
                            </article>
                        </div>

                         <div class="col-md-4 col-sm-6">
                <article>
                    <div class="news-post">
                        <div class="img-cap-effect">
                        <div class="img-box">
                        <img src="images/11.jpg" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">5</a>
                                </div>
                            </div>
                            </article>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <article>
                            <div class="news-post">
                                                                <div class="img-cap-effect">
                    <div class="img-box">
                        <img src="images/final.png" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">View more</a>
                                </div>
                            </div>
                            </article>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <article>
                            <div class="news-post">
                                                                <div class="img-cap-effect">
                    <div class="img-box">
                        <img src="images/celeb.jpg" alt="Awesome Image">
                        <div class="img-caption">
                            <div class="box-holder"></div>
                        </div>
                    </div>
                </div>
                                <div class="post-content-text">




                                    <a class="btn readmore">View more</a>
                                </div>
                            </div>
                            </article>
                        </div>
                   <p>
Light gauge steel framing technology known as FrameTech® is used in all construction projects. Local businesses and emerging contractors are empowered to use our Circle Building System™ as a fast, reliable method to build exceptional quality.</p>
                    </div>
                </div>

            </div>
        </section>
        <!-- End news section -->
    <!-- Purchase Now
            ================================================== -->

  @include('partials.footer')
    </div>
    <!-- End Container -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>

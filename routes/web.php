<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/testimonial', 'StaticPagesController@testimonial');
Route::get('/contact', 'StaticPagesController@contact');
Route::get('/gallery', 'StaticPagesController@gallery');
Route::get('/waterproofing', 'StaticPagesController@waterproofing');
Route::get('/rising', 'StaticPagesController@rising');
Route::get('/painting', 'StaticPagesController@painting');
Route::get('/water', 'StaticPagesController@water');
Route::get('/stealframing', 'StaticPagesController@stealframing');
Route::get('/rope', 'StaticPagesController@rope');
Route::get('/tiling', 'StaticPagesController@tiling');

Route::get('/tile', 'StaticPagesController@tile');
Route::get('/torch', 'StaticPagesController@torch');
Route::get('/ceiling', 'StaticPagesController@ceiling');
Route::get('/roof', 'StaticPagesController@roof');
Route::get('/bitumen', 'StaticPagesController@bitumen');
Route::get('/acrylic', 'StaticPagesController@acrylic');

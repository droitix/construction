
        <!-- footer
            ================================================== -->
        <footer>
            <div class="up-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="footer-widget">
                                <h2>About Us</h2>
                                <p>We are a customer centered company and ready to assist in times of need.</p>
                                <ul class="social-links social-links_mod-a list-inline">
                                                            <li><a target="_blank" href="https://twitter.com/"><i class="icons fa fa-twitter"></i></a></li>
                                                            <li><a target="_blank" href="https://www.facebook.com/"><i class="icons fa fa-facebook"></i></a></li>
                                                            <li><a target="_blank" href="https://www.linkedin.com/"><i class="icons fa fa-linkedin"></i></a></li>
                                                            <li><a target="_blank" href="https://www.google.com/"><i class="icons fa fa-google-plus"></i></a></li>
                                                            <li><a target="_blank" href="https://www.instagram.com/"><i class="icons fa fa-instagram"></i></a></li>
                                                        </ul>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-widget">
                                <h2>Our Services</h2>
                                <ul class="link-list">
                                    <li><a href="<?php echo e(url('/waterproofing')); ?>">Waterproofing</a></li>
                                    <li><a href="<?php echo e(url('/rising')); ?>">Lateral Damp Proofing</a></li>
                                    <li><a href="<?php echo e(url('/painting')); ?>">Preparation and Painting</a></li>
                                    <li><a href="<?php echo e(url('/water')); ?>">Water Resevoirs</a></li>
                                    <li><a href="<?php echo e(url('/sealing')); ?>">Joint Sealing </a></li>
                                    <li><a href="<?php echo e(url('/tiling')); ?>">Specialised Tiling</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-widget">
                                <h2>Quick Links</h2>
                                <ul class="link-list">
                    <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                    <li><a href="<?php echo e(url('/testimonial')); ?>">Testimonials</a></li>
                    <li><a href="<?php echo e(url('/')); ?>">About Us</a></li>
                    <li><a href="<?php echo e(url('/gallery')); ?>">Gallery</a></li>
                    <li><a href="<?php echo e(url('/contact')); ?>">Contact Us</a></li>
                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-widget info-widget">
                                <h2>Info</h2>
                                <p class="first-par">You can contact or visit us during working time.</p>
                                <p><span>Tel 1: </span>0744825608</p>
                                <p><span>Tel 2: </span>0815738603</p>
                                <p><span>Tel 3: </span>0735886591</p>
                                <p><span>Email: </span> roofandwaterproofing2020@gmail.com</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" copyright">
            <div class="container ">
                <div class="pull-left">
                <p class="">
                &copy; Copyright  2020. All rights reserved.
            </p>
            </div>
            <div class="pull-right">
                <p class="">
                Designed By : paulkumz
            </p>
            </div>
            </div></div>


        </footer>
        <!-- End footer -->
<?php /**PATH /home/vagrant/sites/construction/resources/views/partials/footer.blade.php ENDPATH**/ ?>

<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>Gallery</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Playfair|Open+Sans|Montserrat|Roboto+Slab'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/stroke-gap.css" media="screen">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="7stroke/css/pe-icon-7-stroke.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">




</head>
<body>

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f12de627258dc118bee7a98/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


   <?php echo $__env->make('partials.navigation', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Gallery</h2>
                        <ul class="page-depth">
                            <li><a href="index.html">RWS</a></li>
                            <li><a href="about.html">Gallery</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- projects-page section
            ================================================== -->
        <section class="projects-page-section">
            <div class="container">
                <div class="row welcome_heading">
                        <h2>Projects</h2>
                        <p></p>
                </div></div>
                <div class="gallery">
                <div class="container">
                <ul class="filter">
                    <li><a class="active" href="#" data-filter="*">Show All</a></li>
                    <li><a href="#" data-filter=".transport">Perfection</a></li>
                    <li><a href="#" data-filter=".fleet">Excellence</a></li>
                    <li><a href="#" data-filter=".freight">Artistic</a></li>
                    <li><a href="#" data-filter=".heavy">Value</a></li>
                </ul>
                <div class="project-box iso-call col3">
                    <div class="project-post transport heavy">
                        <img src="images/tiling.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post freight">
                        <img src="images/painting.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post transport heavy">
                        <img src="images/rising.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post transport">
                        <img src="images/rising.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post freight heavy">
                        <img src="images/roof.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post fleet">
                        <img src="images/steelframe.jpg.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post transport">
                        <img src="images/torch.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post heavy">
                        <img src="images/acrylic.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                    <div class="project-post fleet">
                        <img src="images/ceiling.jpg" alt="">
                        <div class="hover-box">

                        </div>
                    </div>
                </div>
            </div></section>
        </section>
        <!-- End projects-page section -->

    <!-- Purchase Now
            ================================================== -->




 <?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- End Container -->


    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>
<?php /**PATH /home/vagrant/sites/construction/resources/views/gallery.blade.php ENDPATH**/ ?>
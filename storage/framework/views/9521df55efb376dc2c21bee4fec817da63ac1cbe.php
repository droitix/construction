
<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>RWS | Seamless Gutters</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Playfair|Open+Sans|Montserrat|Roboto+Slab'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/stroke-gap.css" media="screen">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/pe-icon-7-stroke.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="screen">




</head>
<body>

     <?php echo $__env->make('partials.navigation', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Seamless Gutters</h2>
                        <ul class="page-depth">

                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- services-page section
            ================================================== -->
        <section class="services-page-section">
            <div class="container">
                <div class="row">

                    <div class="col-md-9 white-right">
                        <div class="services-wrapp">
                            <div class="services-post">
                                <img src="images/gutter.jpg" alt="">
                                    <div class="project-title ">
                    <h2 class="box-header">Seamless Gutters</h2>

                </div>
                                <p>The seamless chromadek gutters route not only has obvious aesthetic appeal but also has a lifetime durability span. There is no painting and very little maintenance is required to the seamless gutters. The fact that there are no joins in the seamless gutter means that your  seamless gutter can be virtually level but still function. No joins in straight runs also minimise the possibilities of leakage from the seamless gutter.</p>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="section-title-style-2">
                                        </p>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="services-post">
                                        <img src="upload/others/con2.jpg" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End services-page section -->


       <?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- End Container -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>
<?php /**PATH /home/vagrant/sites/construction/resources/views/water.blade.php ENDPATH**/ ?>
 <!-- Container -->
    <div id="container">
        <div class="social-top">

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4"> </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                <div class="col-sm-12 col-md-6 col-lg-6">
                                                                        <ul class="inline">
<li><i class="fa fa-envelope"></i> &nbsp; roofwaterproofing2020@gmail.com</li>
<li>&nbsp; <br>&nbsp; &nbsp;  <i class="fa fa-phone"></i> &nbsp; 0744825608 | 0815738603 | 0735886591</li>
</ul>                               </div>
                <div class="col-sm-12 col-md-6 col-lg-6 text-right">
                                            <ul class="social-icons">
<li><a href="#"><i class="fa fa-facebook"></i> </a></li>
<li><a href="#"><i class="fa fa-twitter"></i> </a></li>
<li><a href="#"><i class="fa fa-linkedin"></i> </a></li>
<li><a href="#"><i class="fa fa-instagram"></i> </a></li>
</ul>                                   </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Header
            ================================================== -->
        <header class="clearfix">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="top-line bottom_header">
                    <div class="container clear_fix">
                    <div class="pull-left logo">
                        <a href="<?php echo e(url('/')); ?>">
                            <img src="images/logo2.png" height="50" alt="construction">
                        </a>
                    </div>
                    <div class="col-md-8 col-xs-12 hidden-md pull-right">
                    <div class="mainmenu-area">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="active" href="<?php echo e(url('/')); ?>">Home</a>
                            </li>


                            <li class="drop"><a href="services.html">Our Services</a>
                            <ul class="dropdown">
                                      <li><a class="active" href="<?php echo e(url('/waterproofing')); ?>">Roofing/Waterproofing</a></li>
                                        <li><a href="<?php echo e(url('/rising')); ?>">Rising/Lateral Damp Proofing</a></li>
                                        <li><a href="<?php echo e(url('/painting')); ?>">Preparation and Painting</a></li>
                                        <li><a href="<?php echo e(url('/water')); ?>">Seamless Gutters</a></li>
                                        <li><a href="<?php echo e(url('/stealframing')); ?>">Steal framing</a></li>

                                </ul>

                            </li>
                            <li><a href="<?php echo e(url('/gallery')); ?>">Gallery</a>

                            </li>
                            <li><a href="#">Testimonials</a>

                            </li>
                            <li><a href="<?php echo e(url('/contact')); ?>">Contact</a></li>
                            <li class="search drop"><a href="#" class="open-search"><i class="fa fa-search"></i></a>
                                <form class="form-search">
                                    <input type="search" placeholder="Search:"/>
                                    <button type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
           </div>
                </div>
                </div>

            </nav>
        </header>
        <!-- End Header -->
<?php /**PATH /home/vagrant/sites/construction/resources/views/partials/navigation.blade.php ENDPATH**/ ?>
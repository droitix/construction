
<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>Construction</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Playfair|Open+Sans|Montserrat|Roboto+Slab'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/stroke-gap.css" media="screen">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/pe-icon-7-stroke.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/css/settings.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css" media="screen">




</head>
<body>

  <?php echo $__env->make('partials.navigation', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Rising / Lateral Damp Proofing</h2>
                        <ul class="page-depth">

                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- services-page section
            ================================================== -->
        <section class="services-page-section">
            <div class="container">
                <div class="row">

                    <div class="col-md-9 white-right">
                        <div class="services-wrapp">
                            <div class="services-post">
                                <img src="images/rising.jpg" alt="">
                                    <div class="project-title ">
                    <h2 class="box-header">Rising / Lateral Damp Proofing</h2>

                </div>
                                <p>Building a home? Renovating? Rising damp tracking your rising anxiety levels? You need to take action.

Damp proofing is a process all too often neglected in the building stage. Unfortunately, the effects are felt down the line by those who end up occupying the home, office or building. The damage caused by dampness – whether rising, falling or lateral – can be extremely costly. And not just to your pocket, but to the health of your family.

We provide a comprehensive damp-proofing solution in the building phase. We also offer treatments to remediate dampness when it does in fact arise.

Damp-proofing
We apply a latex treatment that is usually fitted onto raw brick. This is particularly effective on subterranean areas.

Remediation
If there’s bubbling on the base of the walls, we will strip plasterwork to the raw brick, and fit the latex anti-damp system onto the brick. We will then replaster and paint the areas we worked on (corner to corner).

Maintenance Aid offers a five-year guarantee on our Damp Proofing System.</p>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="section-title-style-2">


                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="services-post">
                                        <img src="upload/others/con2.jpg" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End services-page section -->

 <?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- End Container -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>

    <script type="text/javascript" src="js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>
<?php /**PATH /home/vagrant/sites/construction/resources/views/rising.blade.php ENDPATH**/ ?>